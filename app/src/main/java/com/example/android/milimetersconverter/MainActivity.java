package com.example.android.milimetersconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText milimeters;
    private EditText inches;
    private Button convert;
    private Button exit;

    private static final NumberFormat currencyFormat=
            NumberFormat.getCurrencyInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        milimeters=(EditText)findViewById(R.id.milimeters);
        inches=(EditText)findViewById(R.id.inches);
        convert=(Button)findViewById(R.id.convert);
        exit=(Button)findViewById(R.id.exit);

        convert.setOnClickListener(this);
        exit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==convert){
            final double mm,converts;

            mm=Double.parseDouble(milimeters.getText().toString());
            converts= (mm/25.4);

            inches.setText(Double.toString(converts));
        }
        else if(view==exit){
finish();
        }
    }
}
